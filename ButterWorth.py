"""
import numpy as np
from scipy.fftpack import rfft, irfft, fftfreq

time   = np.linspace(0,10,2000)
signal = np.cos(5*np.pi*time) + np.cos(7*np.pi*time)

W = fftfreq(signal.size, d=time[1]-time[0])
f_signal = rfft(signal)

# If our original signal time was in seconds, this is now in Hz    
cut_f_signal = f_signal.copy()
cut_f_signal[(W<6)] = 0

cut_signal = irfft(cut_f_signal)

import pylab as plt
plt.subplot(221)
plt.plot(time,signal)
plt.subplot(222)
plt.plot(W,f_signal)
plt.xlim(0,10)
plt.subplot(223)
plt.plot(W,cut_f_signal)
plt.xlim(0,10)
plt.subplot(224)
plt.plot(time,cut_signal)
plt.show()
"""
import numpy as np
from scipy.signal import freqz
from scipy.signal import butter, lfilter

#Moving average
def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs          #if fs is 5000 nyq is 2500
    low = lowcut / nyq      #low is 500/2500 or 0.2
    high = highcut / nyq    #high is 1250/2500 is 0.5
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def run():
    
    import matplotlib.pyplot as plt
    from scipy.signal import freqz
    import scipy.io as spio
    """
    D1 = "H:\\4th_Year\EE40098\Python" +"/D1"
    D2 = "H:\\4th_Year\EE40098\Python" +"/D2"
    D3 = "H:\\4th_Year\EE40098\Python" +"/D3"
    D4 = "H:\\4th_Year\EE40098\Python" +"/D4"
    """
    D1 = "c:/Users/erikk/Desktop/Uni Work/Python/D1"
    D2 = "c:/Users/erikk/Desktop/Uni Work/Python/D2"
    D3 = "c:/Users/erikk/Desktop/Uni Work/Python/D3"
    D4 = "c:/Users/erikk/Desktop/Uni Work/Python/D4"
    

    mat =  spio.loadmat(D1, squeeze_me=True)
    mat2 = spio.loadmat(D2, squeeze_me=True)
    mat3 = spio.loadmat(D3, squeeze_me=True)
    mat4 = spio.loadmat(D4, squeeze_me=True)

    #print(mat)

    d = mat['d'] # array
    Class = mat['Class'] # structure containing an array
    Index = mat['Index'] # array of structures

    d2 = mat2['d'] # array
    d3 = mat3['d'] # array
    d4 = mat4['d'] # array
    # split into input (X) and output (y) variables

    #Moving average
    #d = moving_average(d, n=3)
    #d2 = moving_average(d2, n=3)
    #d3 = moving_average(d3, n=3)
    #d4 = moving_average(d4, n=3)

    # Sample rate and desired cutoff frequencies (in Hz).
    fs = 10000
    lowcut = 30
    highcut = 800

    inputs = np.linspace(0, len(d), len(d))
    sineWave = 4* np.sin(inputs*0.001*np.pi)

    dnew = d + sineWave

    sample = d4

    print(len(inputs))
    print(type(len(sample)))

    interations = (len(sample)+2)/1000

    for i in range(int(interations)):


        out  =  butter_bandpass_filter(moving_average(sample), 10, 900, fs, order=3)
        out1  = moving_average(butter_bandpass_filter(sample, 10, 900, fs, order=3))
        out2  = butter_bandpass_filter(sample, 10, 900, fs, order=3)
        out3  = butter_bandpass_filter(sample, 30, 800, fs, order=12)

        
        
        sample1 = out[i*1000:(i+1)*1000]
        sample2 = out1[i*1000:(i+1)*1000]
        sample3 = out2[i*1000:(i+1)*1000]
        sampleOG = sample[i*1000:(i+1)*1000]
        """
        sample1 = out[1439000:1440000]
        sample2 = out1[1439000:1440000]
        sample3 = out2[1439000:1440000]
        sampleOG = sample[1439000:1440000]
        """
        figure, axis = plt.subplots(2, 2)

        # For Sine Function
        axis[0, 0].plot(sample1)
        axis[0, 0].set_title("Top 1")
        
        # For Cosine Function
        axis[0, 1].plot(sample2)
        axis[0, 1].set_title("TOP 2")
        
        # For Tangent Function
        axis[1, 0].plot(sample3)
        axis[1, 0].set_title("TOP 3")
        
        # For Tanh Function
        axis[1, 1].plot(sample)
        axis[1, 1].set_title("Original")

        # Combine all the operations and display
        plt.show()
    

    """
    # Plot the frequency response for a few different orders.
    plt.figure(1)
    plt.clf()
    for order in [3, 6, 9]:
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        w, h = freqz(b, a, worN=2000)
        plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)

    plt.plot([0, 0.5 * fs], [np.sqrt(0.5), np.sqrt(0.5)],
             '--', label='sqrt(0.5)')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.grid(True)
    plt.legend(loc='best')

    # Filter a noisy signal.
    T = 0.05
    nsamples = int(T * fs)
    t = np.linspace(0, T, nsamples, endpoint=False)
    a = 0.02
    f0 = 600.0
    x = 0.1 * np.sin(2 * np.pi * 1.2 * np.sqrt(t))
    x += 0.01 * np.cos(2 * np.pi * 312 * t + 0.1)
    x += a * np.cos(2 * np.pi * f0 * t + .11)
    x += 0.03 * np.cos(2 * np.pi * 2000 * t)
    plt.figure(2)
    plt.clf()
    plt.plot(t, x, label='Noisy signal')

    y = butter_bandpass_filter(x, lowcut, highcut, fs, order=6)
    plt.plot(t, y, label='Filtered signal (%g Hz)' % f0)
    plt.xlabel('time (seconds)')
    plt.hlines([-a, a], 0, T, linestyles='--')
    plt.grid(True)
    plt.axis('tight')
    plt.legend(loc='upper left')

    plt.show()
    """


run()