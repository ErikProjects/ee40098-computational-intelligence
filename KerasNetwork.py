# first neural network with keras make predictions
import matplotlib.pyplot as plt
from numpy import loadtxt
import numpy as np
import scipy.io as spio
import time
import os
import random
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

#Moving average
def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def checkMultiple(array, value):
    counter = 0
    for i in array:
        if i> value:
            counter = counter +1
    return counter

#Function for selection
def selection(dataSet, cutoffVal):
    #print("Max dataset value: " + str(max(dataSet)))
    cutoff = cutoffVal
    #print("Cutoff = "+str(cutoff))
    time.sleep(2)
    activeBit = 0 #Initially Turned Off
    pastFour = [0, 0, 0, 0]
    activeRegion = []

    #FIND ACTIVE PERIOD
    for p in range(len(dataSet)):

        #Sanity
        if dataSet[p]>3:
            pastFour = dataSet[p-4:p]

        #Is active?
        if activeBit ==1:
        #Yes
            #Check cutoff
            if dataSet[p] >= cutoff:
                #Above
                #Check Last 4
                activeBit = 1
                """
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Still Active
                            activeBit = 1
                else:
                        #2 Above
                            #No Longer Active
                            activeBit = 0
                                #Record Active Period
                """
            else:    
                #Below
                #Check Last 4
                activeBit = 0
                """
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Still Active
                            activeBit = 1
                else:
                        #2 Above
                            #No Longer Active
                            activeBit = 0
                            #Record Active Period
                """
        else:
        #No
            #Check cutoff
            if dataSet[p] >= cutoff:
                #Above
                #Check Last 4
                activeBit = 1
                activeRegion.append(p)
                """
                if checkMultiple(pastFour, cutoff)> 2:
                    #3 Above
                        #Active
                        activeBit = 1
                        #Record Start
                        activeRegion.append(p)
                        #print("New" + str(d2[p]) + "started at1: " + str(p))
                        #time.sleep(0.1)
                else:
                    #2 Below
                        #Still Inactive
                        activeBit = 0
                """
            else:
                #Below
                activeBit = 0
                """
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Active
                            activeBit = 1
                            #Record Start
                            activeRegion.append(p)
                            #print("New" + str(d2[p]) + "started at2: " + str(p))
                            #time.sleep(0.1)
                else:
                        #2 Below
                            #Still Inactive
                            activeBit = 0
                """

    #print("Active region: " + str(activeRegion))

    """
    plt.plot(d2)

    # naming the x axis
    plt.xlabel('x - axis')
    # naming the y axis
    plt.ylabel('y - axis')

    # giving a title to my graph
    plt.title("Class =")

    # function to show the plot
    plt.show()
    """

    resultArray =[]
    regions = []
    for l in range(len(activeRegion)):
        #print("L is : " + str(l))
        d_subsec = dataSet[activeRegion[l]-10:activeRegion[l]+30]
            #print("D subsec dimension = " + str(d_subsec.shape))
            #print("Active region is :  " + str(activeRegion))
        newData = d_subsec.reshape(1, 40)
            #print("NEW D subsec dimension = " + str(newData))
            #print("NEW D subsec dimension = " + str(type(newData)))
        regions.append(d_subsec)
        prediction = model.predict(newData, verbose=0)
        i,j = np.unravel_index(prediction.argmax(), prediction.shape)
        resultArray.append(j+1)
        """
        plt.plot(d_subsec)

        # naming the x axis
        plt.xlabel('x - axis')
        # naming the y axis
        plt.ylabel('y - axis')

        # giving a title to my graph
        plt.title("Class =" +str(prediction) + "  " + str(j+1))

        # function to show the plot
        plt.show()
        #tempData = model.predict(d_subsec)
        #print("Temp data: " + str(tempData))
        """
        
        #resultArray.append(tempData)
    #print("Regions type = " + str(type(regions)))
    #resultArray = model.predict(regions)
    #print("Result = " + str(resultArray))
    #print("Result type = " + str(type(resultArray)))
    #print("DATA SET IS : " + str(type(model.modelData())))
    #result = model.nearestNeighbourPredict(resultArray)
    return resultArray, activeRegion

def intToListBin(inputInt):
    if inputInt ==1:
        output = np.array([1, 0, 0, 0, 0])
    elif inputInt ==2:
        output = np.array([0, 1, 0, 0, 0])
    elif inputInt ==3:
        output = np.array([0, 0, 1, 0, 0])
    elif inputInt ==4:
        output = np.array([0, 0, 0, 1, 0])
    elif inputInt ==5:
        output = np.array([0, 0, 0, 0, 1])
    else:
        output = np.array([0, 0, 0, 0, 0])
    
    return output

# load the dataset

D1 = "H:\\4th_Year\EE40098\Python" +"/D1"
D2 = "H:\\4th_Year\EE40098\Python" +"/D2"
D3 = "H:\\4th_Year\EE40098\Python" +"/D3"
D4 = "H:\\4th_Year\EE40098\Python" +"/D4"
"""
D1 = "c:/Users/erikk/Desktop/Uni Work/Python/D1"
D2 = "c:/Users/erikk/Desktop/Uni Work/Python/D2"
D3 = "c:/Users/erikk/Desktop/Uni Work/Python/D3"
D4 = "c:/Users/erikk/Desktop/Uni Work/Python/D4"
"""

mat =  spio.loadmat(D1, squeeze_me=True)
mat2 = spio.loadmat(D2, squeeze_me=True)
mat3 = spio.loadmat(D3, squeeze_me=True)
mat4 = spio.loadmat(D4, squeeze_me=True)

#print(mat)

d = mat['d'] # array
Class = mat['Class'] # structure containing an array
Index = mat['Index'] # array of structures

d2 = mat2['d'] # array
d3 = mat3['d'] # array
d4 = mat4['d'] # array
# split into input (X) and output (y) variables

#Moving average
d = moving_average(d, n=3)
d2 = moving_average(d2, n=3)
d3 = moving_average(d3, n=3)
d4 = moving_average(d4, n=3)

inputs = []
targets = []
for r in range(len(Index)):
    if Class[r] != 0:
        
        #seenClasses.append(Class[r])
        #print("Seen classes: " + str(seenClasses))
        randShift = random.randint(-3, 5)
        d_subsec = d[Index[r]-10+randShift:Index[r]+30+randShift]
        inputs.append(d_subsec.tolist())
        print("Input is: " + str(Class[r]) + "  output is : " + str(intToListBin(Class[r])))
        targets.append(intToListBin(Class[r]))
        """
        plt.plot(d_subsec)

        # naming the x axis
        plt.xlabel('x - axis')
        # naming the y axis
        plt.ylabel('y - axis')

        # giving a title to my graph
        plt.title("Class =" + str(Class[r]))

        # function to show the plot
        plt.show()
        """
       
    else: 
        print("Not needed")

X = np.array([np.array(xi) for xi in inputs])
y = np.array([targets])
Y = np.transpose(y)
i,j,k = y.shape
Y2 = y.reshape(j, k)
print("y = " + str(y.shape))
print("Y = " + str(Y))
print("Y2 = " + str(Y2.shape))
# define the keras model
model = Sequential()
model.add(Dense(12, input_shape=(40,), activation='relu'))
model.add(Dense(80, activation='relu'))
model.add(Dense(120, activation='relu'))
model.add(Dense(5, activation='linear'))
# compile the keras model
model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
# fit the keras model on the dataset
model.fit(X, Y2, epochs=150, batch_size=10, verbose=1)
# make class predictions with the model
predictions = (model.predict(inputs) > 0.5).astype(int)
print("predictions is : " + str(predictions))
# evaluate the keras model
_, accuracy = model.evaluate(X, Y2)

for h in range(len(predictions)):
    print("prediction: " + str(predictions[h]) + "  Actual: " + str(Class[h]))

print('Accuracy: %.2f' % (accuracy*100))


#cwd = os.getcwd()
#cwd = "c:/Users/erikk/Desktop/Uni Work/Python"
cwd = "H:\\4th_Year\EE40098\Python"
print(cwd)
"""
Class1, Index1  = selection(d, 1)
print(Class1)
print("Test 1 Length: " + str(len(Class1)) + " 1 count: " + str(Class1.count(1)) + " 2 count: " + str(Class1.count(2)) + " 3 count: " + str(Class1.count(3)) + " 4 count: " + str(Class1.count(4)) + " 5 count: " + str(Class1.count(5)))
mdic = {"D": d, "Index": Index, "Class": Class}
loc = str(cwd) + "\outputs\D2.mat"
spio.savemat(loc, mdic)
ClassList = Class.tolist()
print("Original Length: " + str(len(ClassList)) + " 1 count: " + str(ClassList.count(1)) + " 2 count: " + str(ClassList.count(2)) + " 3 count: " + str(ClassList.count(3)) + " 4 count: " + str(ClassList.count(4)) + " 5 count: " + str(ClassList.count(5)))
"""
Class2, Index2  = selection(d2, 1)
print("Test 1 Length: " + str(len(Class2)) + " 1 count: " + str(Class2.count(1)) + " 2 count: " + str(Class2.count(2)) + " 3 count: " + str(Class2.count(3)) + " 4 count: " + str(Class2.count(4)) + " 5 count: " + str(Class2.count(5)))
mdic2 = {"D": d2, "Index": Index2, "Class": Class2}
loc2 = str(cwd) + "\outputs\D2.mat"
spio.savemat(loc2, mdic2)

Class3, Index3  = selection(d3, 1)
mdic3 = {"D": d3, "Index": Index3, "Class": Class3}
loc3 = str(cwd) + "\outputs\D3.mat"
spio.savemat(loc3, mdic3)
"""
Class4, Index4  = selection(d4, 1)
mdic4 = {"D": d4, "Index": Index4, "Class": Class4}
loc4 = str(cwd) + "\outputs\D4.mat"
spio.savemat(loc4, mdic4)
"""
"""
success = []
fail = []
for i in range(100):
    val = random.randint(0, len(Class))
    analyse = d[Index[val]-25:Index[val]+45]

    print("D subsec dimension going in = " + str(analyse.shape))
    
    Class2, Index2  = selection(analyse, 0.5)
    if not Class2:
        print("NO OUTPUT DETECTED  In class " + str(Class[val]))
        fail.append(Index2)
    elif Class2[0] == Class[val]:
        success.append(Index2)
    else:
        print("Output =" +str(Class2) + " Class is actually " + str(Class[val]))
        fail.append(Index2)

print("Success = " +str(len(success)) + " Fail = " + str(len(fail)))
"""
#Use on second data set
#Class2, Index2  = selection(d, 0.1)


#Use on second data set
