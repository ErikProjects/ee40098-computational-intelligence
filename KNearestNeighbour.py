# importing the required module
import matplotlib.pyplot as plt
import os
import scipy.io as spio
import time
import numpy as np
# Import necessary modules
from sklearn.neighbors import KNeighborsClassifier


#Pseudo Code
    #Known data

    #Read Index
        #Collect data from d
        #Parse to class
            #Process data
        #Add processed data to data base/ graph

    #Read data stream 0->End
        #Extract data frame from each data point
        #Parse to class
            #Process data
        #Use K-Nearest neighbour to get results
            #Results should look like a string eg: 00000111111000000002222200000

        #Filter false datapoints
        #Construct Index, Class

#Additional helper functions
def checkMultiple(array, value):
    counter = 0
    for i in array:
        if i> value:
            counter = counter +1
    return counter

class SpikeSorting():
    def __init__(self, classes):
        #List of dictionaries
        self.sharedDatabase = np.empty(shape=[0, 7])
        self.knn = KNeighborsClassifier(n_neighbors=7)
        print("Spike soring with "+ str(classes)+ "  classes")

    def addDataPoint(self, data, value):
        print("data added")
        #Should accept a list of raw values
        tempData = self.processData(data, value)
        self.sharedDatabase = np.append(self.sharedDatabase, [tempData], axis=0)
        #print(self.sharedDatabase)
        ##Should add a dictionary to the list of dictionaries
        #self.sharedDatabase.append(tempData)

    def processData(self, data, value):
        #Should accept a list of raw values

        #Make variables
        maxVal = max(data)
        minVal = min(data)
        nintythPercentile = maxVal*0.9
        nCount = 0
        seventythPercentile = maxVal*0.7
        sCount = 0
        fiftythPercentile = maxVal*0.5
        fCount =0
        zCount =0

        #Go value by value
        for p in data:
            if p>nintythPercentile:
                nCount = nCount+1
            elif p>seventythPercentile:
                sCount =sCount+1
            elif p>fiftythPercentile:
                fCount =fCount +1
            elif p<0:
                zCount = zCount+1


        #Common sense additions
        above90 = nCount + sCount + fCount
        above70 = sCount + fCount
        above50 = fCount

        output = [value, maxVal, minVal, above90, above70, above50, zCount]
        """
        output = {
            "class" : value,
            "max" : maxVal,
            "min" : minVal,
            "90Count" : above90,
            "70Count" : above70,
            "50Count" : above50,
            "0Count"  : zCount
        }
        """
        print("data processed")
        #Should return dictionary

        return output

    def judgeData(self, data):
        print("Jundging data")

        #Should accept a list of raw values

        #Make variables
        maxVal = max(data)
        minVal = min(data)
        nintythPercentile = maxVal*0.9
        nCount = 0
        seventythPercentile = maxVal*0.7
        sCount = 0
        fiftythPercentile = maxVal*0.5
        fCount =0
        zCount =0

        #Go value by value
        for p in data:
            if p>nintythPercentile:
                nCount = nCount+1
            elif p>seventythPercentile:
                sCount =sCount+1
            elif p>fiftythPercentile:
                fCount =fCount +1
            elif p<0:
                zCount = zCount+1


        #Common sense additions
        above90 = nCount + sCount + fCount
        above70 = sCount + fCount
        above50 = fCount

        output = [maxVal, minVal, above90, above70, above50, zCount]
        """
        output = {
            "class" : value,
            "max" : maxVal,
            "min" : minVal,
            "90Count" : above90,
            "70Count" : above70,
            "50Count" : above50,
            "0Count"  : zCount
        }
        """
        print("data processed")
        #Should return dictionary

        return output


    def nearestNeighbour(self, values, classes):
        print("running nearest neightbour")
        self.knn.fit(values, classes)

    def nearestNeighbourPredict(self, values):
        return self.knn.predict(values)


    def modelData(self):
        return self.sharedDatabase




# x axis values
x = [1,2,3]
# corresponding y axis values
y = [2,4,1]

cwd = os.getcwd()
print(cwd)

D1 = "H:\\4th_Year\EE40098\Python" +"/D1"
D2 = "H:\\4th_Year\EE40098\Python" +"/D2"
D3 = "H:\\4th_Year\EE40098\Python" +"/D3"
D4 = "H:\\4th_Year\EE40098\Python" +"/D4"
"""

D1 = "c:/Users/erikk/Desktop/Uni Work/Python/D1"
D2 = "c:/Users/erikk/Desktop/Uni Work/Python/D2"
D3 = "c:/Users/erikk/Desktop/Uni Work/Python/D3"
D4 = "c:/Users/erikk/Desktop/Uni Work/Python/D4"
"""
mat = spio.loadmat(D1, squeeze_me=True)
mat2 = spio.loadmat(D2, squeeze_me=True)
mat3 = spio.loadmat(D3, squeeze_me=True)
mat4 = spio.loadmat(D4, squeeze_me=True)
#print(mat)

d = mat['d'] # array
Class = mat['Class'] # structure containing an array
Index = mat['Index'] # array of structures

d2 = mat2['d'] # array
d3 = mat3['d'] # array
d4 = mat4['d'] # array


#Learn from base set
seenClasses = []
model = SpikeSorting(5)
for r in range(len(Index)):
    if Class[r] != 0:
        
        #seenClasses.append(Class[r])
        #print("Seen classes: " + str(seenClasses))
        d_subsec = d[Index[r]-20:Index[r]+40]
        model.addDataPoint(d_subsec, Class[r])
        #print("Data")
        #print(str(model.modelData()))
        # plotting the points 
        #plt.plot(d_subsec)

        # naming the x axis
        #plt.xlabel('x - axis')
        # naming the y axis
        #plt.ylabel('y - axis')

        # giving a title to my graph
        #plt.title("Class =" + str(Class[r]))

        # function to show the plot
        #plt.show()
        #input("Press Enter to continue...")
    else: 
        print("Not needed")
  


#Knearest neighbour
learnedData = model.modelData()
classes = learnedData[:,0]
values = learnedData[:,1:7]
testValues = values[0:10,:]
testClass = classes[0:10]

"""
#ALSO ADD 0 CLASS OTHERWISE IT GETS FUCKED/ MIGHT NOT BE FUCKED
for t in range(200):
    d_subsec = d2[t*10:t*10+60]
    model.addDataPoint(d_subsec, 0)
"""

model.nearestNeighbour(values, classes)
#print("Values")
#print(testValues)
#results = model.judgeData(testValues)
#print("Results are :  " + str(results))

#janky for loop for analysing all results
for dNum in range(3):
    print("Dnum is:  " + str(dNum))
    print("Change the data set based on iteration number")
    if dNum == 1:
        d2 = d3
    elif dNum ==2:
        d2 = d4


    cutoff = max(d2) * 0.3
    print("Cutoff = "+str(cutoff))
    time.sleep(2)
    activeBit = 0 #Initially Turned Off
    pastFour = [0, 0, 0, 0]
    activeRegion = []

    #FIND ACTIVE PERIOD
    for p in range(len(d2)):

        #Sanity
        if d2[p]>3:
            pastFour = d2[p-4:p]

        #Is active?
        if activeBit ==1:
        #Yes
            #Check cutoff
            if d2[p] >= cutoff:
                #Above
                #Check Last 4
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Still Active
                            activeBit = 1
                else:
                        #2 Above
                            #No Longer Active
                            activeBit = 0
                                #Record Active Period
            else:    
                #Below
                #Check Last 4
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Still Active
                            activeBit = 1
                else:
                        #2 Above
                            #No Longer Active
                            activeBit = 0
                            #Record Active Period
        else:
        #No
            #Check cutoff
            if d2[p] >= cutoff:
                #Above
                #Check Last 4
                if checkMultiple(pastFour, cutoff)> 2:
                    #3 Above
                        #Active
                        activeBit = 1
                        #Record Start
                        activeRegion.append(p)
                        #print("New" + str(d2[p]) + "started at1: " + str(p))
                        #time.sleep(0.1)
                else:
                    #2 Below
                        #Still Inactive
                        activeBit = 0
            else:
                #Below
                if checkMultiple(pastFour, cutoff)> 2:
                        #3 Above
                            #Active
                            activeBit = 1
                            #Record Start
                            activeRegion.append(p)
                            #print("New" + str(d2[p]) + "started at2: " + str(p))
                            #time.sleep(0.1)
                else:
                        #2 Below
                            #Still Inactive
                            activeBit = 0

    print("Active region: " + str(activeRegion))



    #for l in range(len(d2)-60):
    resultArray = []
    for l in range(len(activeRegion)):
        d_subsec = d2[activeRegion[l]-20:activeRegion[l]+40]
        tempData = model.judgeData(d_subsec)
        #print("Temp data: " + str(tempData))
        
        resultArray.append(tempData)


    #TESTING KNOWN VALUES
    """
    resultArray = []
    for l in range(len(Class)):
        d_subsec = d[Index[l]-20:Index[l]+40]
        tempData = model.judgeData(d_subsec)
        #print("Temp data: " + str(tempData))
        
        resultArray.append(tempData)
    """
    #print(resultArray)

    result = model.nearestNeighbourPredict(resultArray)

    #print("Active Region:  "+str(activeRegion))
    #print("Index: " + str(Index))
    #print("Results: " + str(result))
    #print("Classes: " + str(Class))
    """
    for l in range(len(activeRegion)):
        d_subsec = d[activeRegion[l]-110:activeRegion[l]+150]
        tempData = model.judgeData(d_subsec)
        #print("Temp data: " + str(tempData))
        
        plt.plot(d_subsec)

        resultArray.append(tempData)
        # naming the x axis
        plt.xlabel('x - axis')
        # naming the y axis
        plt.ylabel('y - axis')

        # giving a title to my graph
        plt.title("Class =" + str(result[l]))
        plt.show()
    """


    mdic = {"D": d2, "Index": activeRegion, "Class": result}

    if dNum == 1:
        loc = str(cwd) + "\outputs\D3.mat"
    elif dNum ==2:
        loc = str(cwd) + "\outputs\D4.mat"
    else:
        loc = str(cwd) + "\outputs\D2.mat"

    print(loc)
    time.sleep(2)
    spio.savemat(loc, mdic)


#VISUAL FEEDBACK
"""
for l in range(len(activeRegion)):
    d_subsec = d2[activeRegion[l]-100:activeRegion[l]+150]
    tempData = model.judgeData(d_subsec)
    #print("Temp data: " + str(tempData))
    
    plt.plot(d_subsec)

    resultArray.append(tempData)
    # naming the x axis
    plt.xlabel('x - axis')
    # naming the y axis
    plt.ylabel('y - axis')

    # giving a title to my graph
    plt.title("PredClass =" + str(result[l]))
    plt.show()
"""
"""
d_subsec = d[0:10000]
plt.plot(d_subsec)

# naming the x axis
plt.xlabel('x - axis')
# naming the y axis
plt.ylabel('y - axis')

# giving a title to my graph
plt.title("Class =" + str(result[l]))


print("Final Result is: " + str(result))

for k in range(100):
    #plotting the points 
    d_subsec = d2[k*100:k*100+60]
    plt.plot(d_subsec)

    # naming the x axis
    plt.xlabel('x - axis')
    # naming the y axis
    plt.ylabel('y - axis')

    # giving a title to my graph
    plt.title("Class =" + str(result[l]))

    # function to show the plot
    plt.show()
"""
#knn = KNeighborsClassifier(n_neighbors=7)


#knn.fit(values, classes)

#print(str(model.modelData()))


#print("predicted  :  " + str(knn.predict(testValues)))
#print(testClass)



