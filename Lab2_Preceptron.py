# Import the NumPy library for matrix math
import numpy
# A single perceptron function
def perceptron(inputs_list, weights_list, bias):
    # Convert the inputs list into a numpy array
    inputs = numpy.array(inputs_list)
    # Convert the weights list into a numpy array
    weights = numpy.array(weights_list)
    # Calculate the dot product
    summed = numpy.dot(inputs, weights)
    # Add in the bias
    summed = summed + bias
    # Calculate output
    # N.B this is a ternary operator, neat huh?
    output = 1 if summed > 0 else 0
    return output
# Our main code starts here
# Test the perceptron

NANDweights = [-0.5, -0.5]
ANDweights = [-0.5, -0.5]
ORweights = [0.5, 0.5]
NANDbias = 0.5
ANDbias = 0.5
ORbias = 0

#print("Weights: ",weights)
#print("Bias: ", bias)
for x1 in range(0,2):
    for y1 in range(0,2):
        inputs = [x1, y1]

        print("Inputs: ", inputs)
        NANDoutput = perceptron(inputs, NANDweights, NANDbias)
        print("Result: ", NANDoutput)

for x1 in range(0,2):
    for y1 in range(0,2):
        inputs = [x1, y1]

        print("Inputs: ", inputs)
        ORoutput = perceptron(inputs, ORweights, ORbias)
        print("Result: ", ORoutput)

for x1 in range(0,2):
    for y1 in range(0,2):
        

        print("Inputs: ", inputs)
        print("Result: ", perceptron(inputs, weights, bias))

"""
# Import the matplotlib pyplot library
# It has a very long name, so import it as the name plt
import matplotlib.pyplot as plt
# Make a new plot (XKCD style)
fig = plt.xkcd()
# Add points as scatters - scatter(x, y, size, color)
# zorder determines the drawing order, set to 3 to make the
# grid lines appear behind the scatter points
plt.scatter(0, 0, s=50, color="red", zorder=3)
plt.scatter(0, 1, s=50, color="red", zorder=3)
plt.scatter(1, 0, s=50, color="red", zorder=3)
plt.scatter(1, 1, s=50, color="green", zorder=3)

#Line 
x = numpy.linspace(-2, 2, 2)
#plt.plot(x,-x+0.5, linestyle = 'dotted')
plt.plot(x,-x+1.5, linestyle = 'dotted')
# Set the axis limits
plt.xlim(-2, 2)
plt.ylim(-2, 2)
# Label the plot
plt.xlabel("Input 1")
plt.ylabel("Input 2")
plt.title("State Space of Input Vector")
# Turn on grid lines

plt.grid(True, linewidth=1, linestyle=":")
# Autosize (stops the labels getting cut off)
plt.tight_layout()
# Show the plot
plt.show()
"""